package com.jtt809.demo.up.business;

import com.jtt809.demo.up.business.impl.*;
import com.jtt809.demo.up.constant.ConstantJtt809;
import com.jtt809.demo.up.pojo.BusinessBean;
import com.jtt809.demo.up.pojo.Response;
import com.jtt809.demo.up.pojo.command.response.ResponseJtt809_0x1200_VehiclePackage;
import com.jtt809.demo.up.pojo.command.response.ResponseJtt809_0x1600_VehiclePackage;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * 业务工厂，实例化业务
 */
@Slf4j
public class BusinessFactory implements Runnable {

    /**
     * 创建队列
     */
    private static final BlockingQueue<BusinessBean> QUEUE = new LinkedBlockingQueue<BusinessBean>(5000);

    /**
     * 业务实现方法类
     */
    public static Map<Integer, Class<? extends IBusinessServer>> businessMap = new HashMap<Integer, Class<? extends IBusinessServer>>();

    static {
        /**
         * 收到：主链路登录请求消息（0x1001）
         * 响应：主链路登录应答消息（0x1002）
         */
        businessMap.put(ConstantJtt809.UP_CONNECT_REQ, ResponseHandlerImpl_0x1001.class);

        /**
         * 收到：主链路连接保持请求消息（0x1005）
         * 响应：主链路连接保持应答消息（0x1006）
         */
        businessMap.put(ConstantJtt809.UP_LINKTEST_REQ, ResponseHandlerImpl_0x1005.class);

        /**
         * 收到：从链路连接应答信息（0x9002）
         */
        businessMap.put(ConstantJtt809.DOWN_CONNECT_RSP, ResponseHandlerImpl_0x9002.class);

        /**
         * 收到：实时上传车辆定位信息消息（0x1202）
         * 响应：接收车辆定位信息数量通知消息（达到一定量时发送通知）
         */
        businessMap.put(ConstantJtt809.UP_EXG_MSG_REAL_LOCATION, ResponseHandlerImpl_0x1202.class);

        /**
         * 收到：车辆定位信息自动补报（0x1203）
         * 响应：接收车辆定位信息数量通知消息（达到一定量时发送通知）
         */
        businessMap.put(ConstantJtt809.UP_EXG_MSG_HISTORY_LOCATION, ResponseHandlerImpl_0x1203.class);

        /**
         * 收到：上报车辆驾驶员身份识别信息应答（0x120A）
         */
        businessMap.put(ConstantJtt809.UP_EXG_MSG_REPORT_DRIVER_INFO_ACK, ResponseHandlerImpl_0x120A.class);

        /**
         * 收到：上传车辆注册信息（0x1201）
         */
        businessMap.put(ConstantJtt809.UP_EXG_MSG_REGISTER, ResponseHandlerImpl_0x1201.class);

        /**
         * 收到：补发车辆静态信息应答（0x1601）
         */
        businessMap.put(ConstantJtt809.UP_BASE_MSG_VEHICLE_ADDED_ACK, ResponseHandlerImpl_0x1601.class);
    }

    /**
     * 有数据时直接从队列的队首取走，无数据时阻塞，在millis秒内有数据，取走，超过millis秒还没数据，返回失败
     *
     * @param millis
     * @return
     * @throws InterruptedException
     */
    public static BusinessBean waitFor(long millis) throws InterruptedException {
        BusinessBean businessBean = QUEUE.poll(millis, TimeUnit.MILLISECONDS);
        if (businessBean == null) {
            log.error("===========> QUEUE.poll 没有取出数据");
        }
        return businessBean;
    }

    /**
     * 设定的等待时间为1s，如果超过1s还没加进去返回false
     *
     * offer(E e, long timeout, TimeUnit unit)：在队尾插入一个元素,，如果队列已满，则进入等待，直到出现以下三种情况：-->阻塞
     * 被唤醒 /等待时间超时 /当前线程被中断
     *
     * @param businessBean
     * @throws InterruptedException
     */
    public static boolean goOn(BusinessBean businessBean) throws InterruptedException {

        boolean b = QUEUE.offer(businessBean, 1000, TimeUnit.MILLISECONDS);
        if (!b) {
            log.error("===========> QUEUE.offer失败：{}", businessBean.getResponse());
        }
        return b;
    }

    @Override
    public void run() {
        while (true) {
            try {
                int size = QUEUE.size();
                if (size == 0) {
                    Thread.sleep(1000);
                    continue;
                }

                log.info("================> 【信息|队列】剩余：{}", size);
                BusinessBean businessBean = waitFor(1000);

                if (businessBean == null) {
                    continue;
                }

                Response msg = businessBean.getResponse();
                ChannelHandlerContext ctx = businessBean.getCtx();

                Class<? extends IBusinessServer> cls = null;
                int dataType = 0;

                switch (msg.getMsgId()) {
                    // 主链路动态信息交换消息
                    case ConstantJtt809.UP_EXG_MSG:
                        // 获取子业务类型
                        if (msg instanceof ResponseJtt809_0x1200_VehiclePackage) {
                            ResponseJtt809_0x1200_VehiclePackage responseJtt8090x1200VehiclePackage = (ResponseJtt809_0x1200_VehiclePackage) msg;
                            dataType = responseJtt8090x1200VehiclePackage.getDataType();
                            cls = businessMap.get(dataType);
                        }
                        break;
                        // 主链路静态信息交换消息
                    case ConstantJtt809.UP_BASE_MSG:
                        // 获取子业务类型
                        if (msg instanceof ResponseJtt809_0x1600_VehiclePackage) {
                            ResponseJtt809_0x1600_VehiclePackage responseJtt8090x1600VehiclePackage = (ResponseJtt809_0x1600_VehiclePackage) msg;
                            dataType = responseJtt8090x1600VehiclePackage.getDataType();
                            cls = businessMap.get(dataType);
                        }
                        break;
                    default:
                        cls = businessMap.get(msg.getMsgId());
                }


                // 未找到实现方法则返回
                if (cls == null) {
                    log.error("=========>【上级平台】不支持的应答类型：0x{}", dataType > 0 ? Integer.toHexString(dataType) : Integer.toHexString(msg.getMsgId()));
                    continue;
                }

                // 实例化类
                IBusinessServer iBusinessServer = cls.newInstance();
                // 执行业务方法
                iBusinessServer.businessHandler(ctx, msg);
            } catch (Exception e) {
                log.error("================> 【信息|队列】取出异常", e);
            }
        }
    }
}
