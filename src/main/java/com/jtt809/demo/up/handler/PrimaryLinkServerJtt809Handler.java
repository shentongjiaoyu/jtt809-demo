package com.jtt809.demo.up.handler;

import cn.hutool.json.JSONUtil;
import com.jtt809.demo.up.business.BusinessFactory;
import com.jtt809.demo.up.constant.ConstantJtt809;
import com.jtt809.demo.up.pojo.BusinessBean;
import com.jtt809.demo.up.pojo.Response;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.util.internal.PlatformDependent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.net.InetSocketAddress;
import java.util.concurrent.atomic.AtomicLong;

/**
 * <p>主链路处理器</p>
 *
 * @author Allen Yang
 * @datetime 2021/9/8 17:22
 */
@Slf4j
public class PrimaryLinkServerJtt809Handler extends SimpleChannelInboundHandler<Response> {

    /**
     * 心跳丢失次数
     */
    private int counter = 0;

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        // 移除已记录上级平台地址与链接
        InetSocketAddress remoteAddress = (InetSocketAddress) ctx.channel().remoteAddress();
        ConstantJtt809.UP_PLATFORM.remove(remoteAddress.toString());
        super.channelInactive(ctx);

        // netty框架bug，
        // io.netty.channel.nio.AbstractNioByteChannel.NioByteUnsafe.read方法监听关闭连接的时候申请的ByteBuf对象释放后
        // 没有把DIRECT_MEMORY_COUNTER变量减少
        decrementMemoryCounter(PooledByteBufAllocator.DEFAULT.chunkSize());
    }

    public static void decrementMemoryCounter(int capacity) {
        try {
            if (PlatformDependent.usedDirectMemory() <= 0) {
                return;
            }
            Field field = ReflectionUtils.findField(PlatformDependent.class, "DIRECT_MEMORY_COUNTER");
            field.setAccessible(true);
            AtomicLong directMemoryCounter = (AtomicLong) field.get(PlatformDependent.class);
            directMemoryCounter.addAndGet((long)(-capacity));
        } catch (Exception e) {
            log.error("DIRECT_MEMORY_COUNTER减少异常：{}", e.getMessage());
        }
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        super.channelRead(ctx, msg);
        this.counter = 0;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Response msg) throws Exception {
        // 收到消息直接打印输出
        log.info("=====> 【上级平台|接收|{}】指令 = 0x{} ， 数据 = {}", ((InetSocketAddress) ctx.channel().remoteAddress()).toString(), Integer.toHexString(msg.getMsgId()), JSONUtil.toJsonStr(msg));

        // 开启线程执行业务方法
        // ThreadUtil.execute(new BusinessFactory(ctx, msg));
        BusinessFactory.goOn(new BusinessBean(ctx,msg));
    }

    /**
     * 覆盖 channelActive 方法 在channel被启用的时候触发 (在建立连接的时候)
     *
     * @param ctx
     * @throws Exception
     */
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        // 记录上级平台地址与链接
        InetSocketAddress remoteAddress = (InetSocketAddress) ctx.channel().remoteAddress();
        ConstantJtt809.UP_PLATFORM.put(remoteAddress.toString(), ctx);
        super.channelActive(ctx);
        this.counter = 0;
    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        if (evt instanceof IdleStateEvent) {
            IdleStateEvent event = (IdleStateEvent) evt;
            if (event.state().equals(IdleState.READER_IDLE)) {
                InetSocketAddress remoteAddress = (InetSocketAddress) ctx.channel().remoteAddress();
                // 空闲40s之后触发 (心跳包丢失)
                if (counter >= 3) {
                    // 连续丢失3个心跳包 (断开连接)
                    ctx.channel().close().sync();
                    log.info("已与 {} 断开连接", remoteAddress.toString());
                } else {
                    counter++;
                    log.info("{} 丢失了第 {} 个心跳包", remoteAddress.toString(), counter);
                }
            }

        }
    }
}
