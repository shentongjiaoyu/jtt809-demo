package com.jtt809.demo.up.handler.initializer;

import com.jtt809.demo.up.codec.DecoderJtt809;
import com.jtt809.demo.up.codec.EncoderJtt809;
import com.jtt809.demo.up.handler.SlaveLinkServerJtt809Handler;
import com.jtt809.demo.up.pojo.BasePackage;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.timeout.IdleStateHandler;
import lombok.Data;

import java.util.concurrent.TimeUnit;

/**
 * <p>从链路初始化</p>
 *
 * @author Allen Yang
 * @datetime 2021/9/8 16:57
 */
@Data
public class SlaveLinkServerJtt809Initializer extends ChannelInitializer<SocketChannel> {

    private String ip;

    private int port;

    private int verifyCode;

    public SlaveLinkServerJtt809Initializer(String ip, int port, int verifyCode) {
        this.ip = ip;
        this.port = port;
        this.verifyCode = verifyCode;
    }

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline channelPipeline = ch.pipeline();

        //粘包分隔符
        final ByteBuf delimiter = Unpooled.buffer(1);
        delimiter.writeByte(BasePackage.MSG_END_FLAG);
        channelPipeline.addLast("delimiter", new DelimiterBasedFrameDecoder(1024, delimiter));

        // 打印日志信息
//        channelPipeline.addLast("loging", new LoggingHandler(LogLevel.INFO));

        // 解码 和 编码
        channelPipeline.addLast("decoder", new DecoderJtt809());
        channelPipeline.addLast("encoder", new EncoderJtt809());

        // 心跳检测
        channelPipeline.addLast("timeout", new IdleStateHandler(0, 140, 0, TimeUnit.SECONDS));

        // 客户端的逻辑
        channelPipeline.addLast("handler", new SlaveLinkServerJtt809Handler(ip, port, verifyCode));
    }
}