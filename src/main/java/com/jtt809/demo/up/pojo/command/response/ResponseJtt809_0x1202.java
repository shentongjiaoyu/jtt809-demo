package com.jtt809.demo.up.pojo.command.response;

import com.jtt809.demo.up.pojo.BasePackage;
import com.jtt809.demo.up.pojo.Location;
import com.jtt809.demo.up.util.BCDUtil;
import com.jtt809.demo.up.util.HexBytesUtil;
import com.jtt809.demo.up.util.TimeUtil;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * 车辆动态信息交换业务
 * 链路类型：主链路
 * 消息方向：下级平台往上级平台
 * 业务数据类型标识：UP_EXG_MSG
 * 描述：下级平台向上级平台发送车辆动态信息交换业务数据包
 *
 * 具体描述：
 * 实时上传车辆定位信息消息
 * 子业务类型标识：UP_EXG_MSG_REAL_LOCATION
 * 描述：主要描述车辆的实时定位信息
 */
@Data
@Slf4j
public class ResponseJtt809_0x1202 extends ResponseJtt809_0x1200_VehiclePackage {

    /**
     * 车辆定位信息
     */
    private Location location;

    @Override
    protected void decodeDataImpl(ByteBuf buf) {
        this.location = new Location();

        if (isJtt809Version2019()) {
            this.location.setEncrypt(buf.readByte());
            this.location.setLocationDataLength((int) buf.readUnsignedInt());

            this.location.setAlarm(buf.readUnsignedInt());
            this.location.setState(buf.readUnsignedInt());
            this.location.setLat((buf.readUnsignedInt() / 1000000d));
            this.location.setLon((buf.readUnsignedInt() / 1000000d));
            this.location.setAltitude(buf.readUnsignedShort());
            this.location.setVec1(buf.readUnsignedShort() * 10);
            this.location.setDirection(buf.readUnsignedShort());

            try {
                String datetimeStr = BCDUtil.bcd2Str(Unpooled.copiedBuffer(buf.readBytes(6)).array());
                Date date = TimeUtil.parseGpsBcdUnsignedDatetime(datetimeStr);
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));//东八区
                calendar.setTime(date);
                this.location.setYear(calendar.get(Calendar.YEAR));
                this.location.setMonth(calendar.get(Calendar.MONTH) + 1);
                this.location.setDay(calendar.get(Calendar.DATE));
                this.location.setHour(calendar.get(Calendar.HOUR_OF_DAY));
                this.location.setMinute(calendar.get(Calendar.MINUTE));
                this.location.setSecond(calendar.get(Calendar.SECOND));
            } catch (Exception e) {
                log.error("2019版定位时间解析异常",e);
            }

            this.location.setPlatformId1(buf.readBytes(11).toString(BasePackage.DEFAULT_CHARSET_GBK).trim());
            this.location.setAlarm1(buf.readUnsignedInt());
            this.location.setPlatformId2(buf.readBytes(11).toString(BasePackage.DEFAULT_CHARSET_GBK).trim());
            this.location.setAlarm2(buf.readUnsignedInt());
            this.location.setPlatformId3(buf.readBytes(11).toString(BasePackage.DEFAULT_CHARSET_GBK).trim());
            this.location.setAlarm3(buf.readUnsignedInt());
        } else {
            this.location.setEncrypt(buf.readByte());
            this.location.setDay(buf.readByte());
            this.location.setMonth(buf.readByte());
            byte[] yearBytes = new byte[2];
            buf.readBytes(yearBytes);
            int year = Integer.parseInt(HexBytesUtil.bytesToHex(yearBytes),16);
            this.location.setYear(year);
            this.location.setHour(buf.readByte());
            this.location.setMinute(buf.readByte());
            this.location.setSecond(buf.readByte());
            this.location.setLon((buf.readUnsignedInt() / 1000000d));
            this.location.setLat((buf.readUnsignedInt() / 1000000d));
            this.location.setVec1(buf.readUnsignedShort());
            this.location.setVec2(buf.readUnsignedShort());
            this.location.setVec3(buf.readUnsignedInt());
            this.location.setDirection(buf.readUnsignedShort());
            this.location.setAltitude(buf.readUnsignedShort());
            this.location.setState(buf.readUnsignedInt());
            this.location.setAlarm(buf.readUnsignedInt());
        }
    }
}
