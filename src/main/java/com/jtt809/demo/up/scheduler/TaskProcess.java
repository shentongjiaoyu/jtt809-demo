package com.jtt809.demo.up.scheduler;

import com.jtt809.demo.up.util.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Map;

/**
 * <p>定时任务处理器</p>
 *
 * @author Allen Yang
 * @datetime 2021/9/15 13:45
 */
@Component
@Slf4j
public class TaskProcess {

    /**
     * 批量更新车辆最新定位信息
     */
    public void updateLocations() {
        Map locationMap = RedisUtil.RedisMap.entries(RedisUtil.TYPE_ID_RELATE_LOCATION_MAP);
        if (!CollectionUtils.isEmpty(locationMap)) {
            log.info("批量更新车辆最新定位信息{}", locationMap);
        }
    }

}
