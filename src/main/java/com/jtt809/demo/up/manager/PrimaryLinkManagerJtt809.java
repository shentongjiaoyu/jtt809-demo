package com.jtt809.demo.up.manager;

import com.jtt809.demo.up.config.Jtt809Config;
import com.jtt809.demo.up.handler.initializer.PrimaryLinkServerJtt809Initializer;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.WriteBufferWaterMark;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>主链路启动管理器</p>
 *
 * @author Allen Yang
 * @datetime 2021/9/8 17:26
 */
@Slf4j
@Data
public class PrimaryLinkManagerJtt809 implements Runnable {

    /**
     * 绑定IP
     */
    private String ip;

    /**
     * 绑定端口
     */
    private int port;

    /**
     * 构造函数
     *
     * @param ip
     * @param port
     */
    public PrimaryLinkManagerJtt809(String ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    /**
     * 启动服务端
     */
    public void run() {
        //boss线程监听端口，worker线程负责数据读写
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap serverBootstrap = new ServerBootstrap();
            //初始化的主从"线程池"
            serverBootstrap.group(bossGroup, workerGroup)
                            .channel(NioServerSocketChannel.class)
                            // 保持长连接
                            .childOption(ChannelOption.SO_KEEPALIVE,true)
                            // 标识当服务器请求处理线程全满时，用于临时存放已完成三次握手的请求的队列的最大长度
                            .option(ChannelOption.SO_BACKLOG, 1024)
                            //读缓冲区为nM
                            .option(ChannelOption.SO_RCVBUF, 10 * 1024 * 1024)
                            // 接收发送数据的缓存大小nM
                            .childOption(ChannelOption.WRITE_BUFFER_WATER_MARK, new WriteBufferWaterMark(1,10 * 1024 * 1024))
                            .childHandler(new PrimaryLinkServerJtt809Initializer());

            // 绑定端口，同步等待成功
            ChannelFuture channelFuture = serverBootstrap.bind(this.ip, this.port).sync();
            log.info("\n\n\n==========> {}版本上级平台服务端启动成功。绑定IP：{}，绑定端口：{}，来源：{}，从链路启动：{}\n\n\n",
                    Jtt809Config.JTT809_VERSION,
                    this.ip,
                    this.port,
                    Jtt809Config.JTT809_SOURCE,
                    Jtt809Config.JTT809_SLAVE_CREATE);

            // 等待服务器监听端口关闭
            channelFuture.channel().closeFuture().sync();
        } catch (Exception e) {
            log.info("\n\n\n==========> {}版本上级平台服务端启动失败。绑定IP：{}，绑定端口：{}，来源：{}，从链路启动：{}\n\n\n",
                    Jtt809Config.JTT809_VERSION,
                    this.ip,
                    this.port,
                    Jtt809Config.JTT809_SOURCE,
                    Jtt809Config.JTT809_SLAVE_CREATE);
            log.error("==========> 上级平台服务端启动出错。错误：{}", e);
        } finally {
            // 释放线程池资源
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }
}
