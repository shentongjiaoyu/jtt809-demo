package com.jtt809.demo.up.manager;

import com.jtt809.demo.up.handler.initializer.SlaveLinkServerJtt809Initializer;
import com.jtt809.demo.up.pojo.command.request.RequestJtt809_0x9001;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * <p>从链路启动管理器</p>
 *
 * @author Allen Yang
 * @datetime 2021/9/8 17:25
 */
@Slf4j
@Data
public class SlaveLinkManagerJtt809 {

    private static Map<String, Integer> SLAVE_LINK_FAIL_MAP = new HashMap();
    private static final int SLAVE_LINK_FAIL_MAX_COUNT = 5;

    private Bootstrap bootstrap;

    private Channel channel;

    /**
     * ip
     */
    private String ip;

    /**
     * 端口
     */
    private int port;

    /**
     * 校验码
     */
    private int verifyCode;

    /**
     * 构造函数
     *
     * @param ip
     * @param port
     * @param verifyCode
     */
    public SlaveLinkManagerJtt809(String ip, int port, int verifyCode) {
        this.ip = ip;
        this.port = port;
        this.verifyCode = verifyCode;
    }

    /**
     * 启动客户端
     */
    public Channel start() {
        EventLoopGroup eventLoopGroup = new NioEventLoopGroup();
        try {
            bootstrap = new Bootstrap();
            bootstrap.group(eventLoopGroup)
                    .option(ChannelOption.SO_KEEPALIVE, true)
                    .channel(NioSocketChannel.class)
                    .handler(new SlaveLinkServerJtt809Initializer(ip, port, verifyCode));

            // 连接服务端
            ChannelFuture channelFuture = bootstrap.connect(ip, port);
            channelFuture.addListener(new ChannelFutureListener() {
                public void operationComplete(ChannelFuture future) throws Exception {
                    String host = ip + ":" + port;
                    int failCount = SLAVE_LINK_FAIL_MAP.getOrDefault(host, 0);
                    if (failCount >= SLAVE_LINK_FAIL_MAX_COUNT) {
                        log.info("======> 【从链路|从链路连接保持请求消息】下级平台{}第{}次连接不上，断开重连操作......", host, failCount);
                        SLAVE_LINK_FAIL_MAP.remove(host);
                        return;
                    }
                    if (!future.isSuccess()) {
                        log.error("", future);
                        final EventLoop loop = future.channel().eventLoop();
                        int finalFailCount = failCount + 1;
                        loop.schedule(new Runnable() {
                            public void run() {
                                SLAVE_LINK_FAIL_MAP.put(host, finalFailCount);
                                log.info("======> 【从链路|从链路连接保持请求消息】下级平台{}第{}次连接不上，开始重连操作......", host, finalFailCount);
                                start();
                            }
                        }, 5L, TimeUnit.SECONDS);
                    } else {
                        channel = future.channel();
                        log.info("======> 【从链路|从链路连接保持请求消息】下级平台连接成功......");
                    }
                }
            });

            // 延迟
            Thread.sleep(500);

            // 从链路链接请求
            RequestJtt809_0x9001 requestJtt8090x9001 = new RequestJtt809_0x9001();
            requestJtt8090x9001.setVerifyCode(verifyCode);
            channel.writeAndFlush(requestJtt8090x9001);

        } catch (Exception e) {
            log.error("===========> 从链路链接请求异常：{}", e);
        }

        return this.channel;
    }
}
