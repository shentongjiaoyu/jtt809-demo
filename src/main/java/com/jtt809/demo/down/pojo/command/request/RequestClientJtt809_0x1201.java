package com.jtt809.demo.down.pojo.command.request;

import com.jtt809.demo.up.constant.ConstantJtt809;
import io.netty.buffer.ByteBuf;
import lombok.Getter;
import lombok.Setter;

/**
 * 车辆动态信息交换业务
 * 链路类型：主链路
 * 消息方向：下级平台往上级平台
 * 业务数据类型标识：UP_EXG_MSG
 * 描述：下级平台向上级平台发送车辆动态信息交换业务数据包
 *
 * 具体描述：
 * 上传车辆注册信息
 * 子业务类型标识：UP_EXG_MSG_REGISTER
 * 描述：上传车辆注册信息
 */
@Setter
@Getter
public class RequestClientJtt809_0x1201 extends RequestClientJtt809_0x1200_VehiclePackage {

    /**
     * 平台唯一编号
     * 11 byte
     */
    private String platformId;

    /**
     * 车载终端厂商唯一编号
     * 11 byte
     */
    private String producerId;

    /**
     * 车载终端型号，不足20位时以“\0”终结
     * 20 byte
     */
    private String terminalModeType;

    /**
     * 车载终端通讯模块IMEI码
     * 15 byte
     */
    private String imeiId;

    /**
     * 车载终端编号，大写字母和数字组成
     * 7 byte
     */
    private String terminalId;

    /**
     * 车载终端SIM卡电话号码。号码不足12位，则在前补充数字0
     * 12 byte
     */
    private String terminalSimCode;

    public RequestClientJtt809_0x1201() {
        super(ConstantJtt809.UP_EXG_MSG_REGISTER);
        if (isJtt809Version2019()) {
            super.setDataLength(110);
        } else {
            super.setDataLength(61);
        }
    }

    @Override
    protected void encodeDataImpl(ByteBuf buf) {
        //11 byte
        buf.writeBytes(getBytesWithLengthAfter(11, getPlatformId().getBytes()));
        //11 byte
        buf.writeBytes(getBytesWithLengthAfter(11, getProducerId().getBytes()));
        if (isJtt809Version2019()) {
            //30 byte
            buf.writeBytes(getBytesWithLengthAfter(30, getTerminalModeType().getBytes()));
            //15 byte
            buf.writeBytes(getBytesWithLengthAfter(15, getImeiId().getBytes()));
            //30 byte
            buf.writeBytes(getBytesWithLengthAfter(30, getTerminalId().getBytes()));
            //13 byte
            buf.writeBytes(getBytesWithLengthAfter(13, getTerminalSimCode().getBytes()));
        } else {
            //20 byte
            buf.writeBytes(getBytesWithLengthAfter(20, getTerminalModeType().getBytes()));
            //7 byte
            buf.writeBytes(getBytesWithLengthAfter(7, getTerminalId().getBytes()));
            //12 byte
            buf.writeBytes(getBytesWithLengthAfter(12, getTerminalSimCode().getBytes()));
        }
    }
}
