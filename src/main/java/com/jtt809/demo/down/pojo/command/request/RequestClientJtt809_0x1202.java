package com.jtt809.demo.down.pojo.command.request;

import com.jtt809.demo.up.constant.ConstantJtt809;
import com.jtt809.demo.up.pojo.Location;
import com.jtt809.demo.up.util.BCDUtil;
import com.jtt809.demo.up.util.TimeUtil;
import com.sun.deploy.util.StringUtils;
import io.netty.buffer.ByteBuf;
import lombok.Getter;
import lombok.Setter;

import java.text.ParseException;
import java.util.Arrays;

/**
 * 车辆动态信息交换业务
 * 链路类型：主链路
 * 消息方向：下级平台往上级平台
 * 业务数据类型标识：UP_EXG_MSG
 * 描述：下级平台向上级平台发送车辆动态信息交换业务数据包
 *
 * 具体描述：
 * 实时上传车辆定位信息消息
 * 子业务类型标识：UP_EXG_MSG_REAL_LOCATION
 * 描述：主要描述车辆的实时定位信息
 */
@Setter
@Getter
public class RequestClientJtt809_0x1202 extends RequestClientJtt809_0x1200_VehiclePackage {

    /**
     * 车辆定位信息
     */
    private Location location;


    public RequestClientJtt809_0x1202() {
        super(ConstantJtt809.UP_EXG_MSG_REAL_LOCATION);
        if (isJtt809Version2019()) {
            super.setDataLength(79);
        } else {
            super.setDataLength(28);
        }
    }

    @Override
    protected void encodeDataImpl(ByteBuf buf) {
        if (isJtt809Version2019()) {
            // 1 byte
            buf.writeByte(location.getEncrypt());
            // 4 byte
            buf.writeInt(location.getLocationDataLength());
            // 报警状态 4 byte
            buf.writeInt((int) location.getAlarm());
            // 车辆状态 4 byte
            buf.writeInt((int) location.getState());
            // 纬度 4 byte
            buf.writeInt(formatLonLat(location.getLat()));
            // 经度 4 byte
            buf.writeInt(formatLonLat(location.getLon()));
            // 海拔 2 byte
            buf.writeShort(location.getAltitude());
            // 速度 2 byte
            buf.writeShort(location.getVec1());
            // 方向 2 byte
            buf.writeShort(location.getDirection());

            //时间 6 byte
            String datetime = StringUtils.join(Arrays.asList(String.valueOf(location.getYear()),
                    assistTime(location.getMonth()),
                    assistTime(location.getDay()),
                    assistTime(location.getHour()),
                    assistTime(location.getMinute()),
                    assistTime(location.getSecond())), "");

            try {
                datetime = TimeUtil.formatGpsBcdUnsignedNowDatetime(TimeUtil.parseUnsignedDatetime(datetime));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            buf.writeBytes(getBytesWithLengthAfter(6, BCDUtil.str2Bcd(datetime)));

            //平台编号1 11byte
            buf.writeBytes(getBytesWithLengthAfter(11, location.getPlatformId1().getBytes()));
            //报警状态1 4byte
            buf.writeInt((int) location.getAlarm1());

            //平台编号2 11byte
            buf.writeBytes(getBytesWithLengthAfter(11, location.getPlatformId2().getBytes()));
            //报警状态2 4byte
            buf.writeInt((int) location.getAlarm2());

            //平台编号3 11byte
            buf.writeBytes(getBytesWithLengthAfter(11, location.getPlatformId3().getBytes()));
            //报警状态3 4byte
            buf.writeInt((int) location.getAlarm3());

        } else {
            // 1 byte
            buf.writeByte(location.getEncrypt());

            // 日月年 dmyy 4 byte
            buf.writeByte((byte) location.getDay());
            buf.writeByte((byte) location.getMonth());
            String hexYear = String.format("%04x",location.getYear());
            buf.writeBytes(hexStringToByte(hexYear));

            // 时分秒
            buf.writeByte((byte) location.getHour());
            buf.writeByte((byte) location.getMinute());
            buf.writeByte((byte) location.getSecond());

            // 经度 4 byte
            buf.writeInt(formatLonLat(location.getLon()));
            // 纬度 4 byte
            buf.writeInt(formatLonLat(location.getLat()));

            // 速度 2 byte
            buf.writeShort(location.getVec1());
            // 行驶记录速度 2 byte
            buf.writeShort(location.getVec2());
            // 车辆当前总里程数 4 byte
            buf.writeInt((int) location.getVec3());

            // 方向 2 byte
            buf.writeShort(location.getDirection());

            // 海拔 2 byte
            buf.writeShort(location.getAltitude());

            // 车辆状态 4 byte
            buf.writeInt((int) location.getState());

            // 报警状态 4 byte
            buf.writeInt((int) location.getAlarm());
        }

    }

}
