package com.jtt809.demo.down.pojo.command.request;

import com.jtt809.demo.up.config.Jtt809Config;
import com.jtt809.demo.up.constant.ConstantJtt809;
import com.jtt809.demo.up.pojo.BasePackage;
import io.netty.buffer.ByteBuf;
import lombok.Data;

@Data
public class RequestClientJtt809_0x1001 extends BasePackage {

    /**
     * 用户名
     */
    private int userId;

    /**
     * 密码
     */
    private String password;

    /**
     * 下级平台接入码，上级平台给下级平台分配唯一标识码
     */
    protected long msgGesscenterId;

    /**
     * 下级平台提供对应的从链路服务端 IP 地址
     */
    private String downLinkIp;

    /**
     * 下级平台提供对应的从链路服务器端口号
     */
    private int downLinkPort;

    /**
     * 构造函数
     */
    public RequestClientJtt809_0x1001() {
        super(ConstantJtt809.UP_CONNECT_REQ);
        if (isJtt809Version2019()) {
            this.msgBodyLength = 50;
            this.msgGesscenterId = Jtt809Config.JTT809_FACTORY_ACCESS_CODE;
        } else {
            this.msgBodyLength = 46;
        }
    }

    @Override
    protected void encodeImpl(ByteBuf buf) {
        buf.writeInt(getUserId());
        buf.writeBytes(getBytesWithLengthAfter(8, getPassword().getBytes()));

        if (isJtt809Version2019()) {
            buf.writeInt((int) getMsgGesscenterId());
        }

        buf.writeBytes(getBytesWithLengthAfter(32, getDownLinkIp().getBytes()));
        buf.writeShort(getDownLinkPort());
    }
}
